namespace Plex.Objects.PlexFS
{
    public enum OpenMode
    {
        Open,
        OpenOrCreate
    }
}

