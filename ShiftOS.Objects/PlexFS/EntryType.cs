namespace Plex.Objects.PlexFS
{
	public enum EntryType
	{
		NONEXISTENT,
		FILE,
		DIRECTORY
	}
}

